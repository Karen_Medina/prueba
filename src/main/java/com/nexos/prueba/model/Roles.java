package com.nexos.prueba.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.nexos.prueba.dto.RolesDTO;

import lombok.Data;

@Data
@Entity
@Table(name = "ROLES")
public class Roles implements Serializable{
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@Column(name = "nombreRol")
	private String nombreRol;
	
	public Roles() {
		
	}
	
	public Roles(RolesDTO rolesDTO) {
		this.id = rolesDTO.getId();
		this.nombreRol = rolesDTO.getNombreRol();
	}
}
