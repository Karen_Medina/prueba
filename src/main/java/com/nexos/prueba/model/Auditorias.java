package com.nexos.prueba.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import com.nexos.prueba.dto.AuditoriasDTO;

import lombok.Data;

@Data
@Entity
@Table(name = "AUDITORIAS")
public class Auditorias implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@Column(name = "tipo")
	private String tipo;
	
	@Column(name = "fecha")
	private Date fecha;
	
	@Column(name = "metodoConsumido")
	private String metodoConsumido;
	
	@Size(max = 4000)
	@Column(name = "trama")
	private String trama;
	
	@Column(name = "estado")
	private String estado;
	
	public Auditorias () {
		
	}
	
	public Auditorias (AuditoriasDTO auditoriasDTO) {
		this.id = auditoriasDTO.getId();
		this.tipo = auditoriasDTO.getTipo();
		this.fecha = auditoriasDTO.getFecha();
		this.metodoConsumido = auditoriasDTO.getMetodoConsumido();
		this.trama = auditoriasDTO.getTrama();
		this.estado = auditoriasDTO.getEstado();
	}
}
