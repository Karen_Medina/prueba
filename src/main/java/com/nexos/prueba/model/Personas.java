package com.nexos.prueba.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.nexos.prueba.dto.PersonasDTO;

import lombok.Data;

@Data
@Entity
@Table(name = "PERSONAS")
public class Personas implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@Column(name = "cedula")
	private String cedula;
	
	@Column(name = "nombres")
	private String nombres;
	
	@Column(name = "apellidos")
	private String apellidos;
	
	@Column(name = "edad")
	private Integer edad;
	
	@Column(name = "direccion")
	private String direccion;
	
	@Column(name = "telefono")
	private String telefono;
	
	@Column(name = "fechaNacimiento")
	private String fechaNacimiento;
	
	@Column(name = "nombreMadre")
	private String nombreMadre;
	
	@Column(name = "nombrePadre")
	private String nombrePadre;
	
	@ManyToOne
	@JoinColumn(name = "ID_Rol", nullable = false)
	private Roles roles;
	
	public Personas() {
		
	}
	
	public Personas(PersonasDTO personasDTO) {
		this.id = personasDTO.getId();
		this.cedula = personasDTO.getCedula();
		this.nombres = personasDTO.getNombres();
		this.apellidos = personasDTO.getApellidos();
		this.edad = personasDTO.getEdad();
		this.direccion = personasDTO.getDireccion();
		this.telefono = personasDTO.getTelefono();
		this.fechaNacimiento = personasDTO.getFechaNacimiento();
		this.nombreMadre = personasDTO.getNombreMadre();
		this.nombrePadre = personasDTO.getNombrePadre();
		this.roles = new Roles(personasDTO.getRolesDTO());
	}
}
