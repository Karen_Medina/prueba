package com.nexos.prueba.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.nexos.prueba.model.Auditorias;

@Repository
public interface IAuditoriasDAO extends JpaRepository<Auditorias, Integer> {

}
