package com.nexos.prueba.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.nexos.prueba.model.Nucleo;

@Repository
public interface INucleoDAO extends JpaRepository<Nucleo, Integer> {
	
	/*
	 * Metodo que realiza busqueda en la tabla Nucleos, 
	 * recibiendo como parametro el hijo
	 */
	Nucleo findByHijo(String hijo);

}
