package com.nexos.prueba.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.nexos.prueba.model.Personas;

@Repository
public interface IPersonasDAO extends JpaRepository<Personas, Integer> {

	/*
	 * Metodo que realiza busqueda en la tabla Personas, 
	 * recibiendo como parametro la cedula de la persona
	 */
	Personas findByCedula(String cedula);
	
	/*
	 * Metodo que realiza busqueda en la tabla Personas, 
	 * recibiendo como parametro el id de la persona
	 */
	Personas findByid(Integer id);
}
