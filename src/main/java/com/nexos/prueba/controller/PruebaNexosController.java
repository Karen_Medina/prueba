package com.nexos.prueba.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.nexos.prueba.constant.Constant;
import com.nexos.prueba.dto.MensajeDTO;
import com.nexos.prueba.dto.PersonasDTO;
import com.nexos.prueba.model.Personas;
import com.nexos.prueba.service.IAuditoriaService;
import com.nexos.prueba.service.IPersonaService;

@RestController
@RequestMapping("/nexos-software")
public class PruebaNexosController {

	/*Objeto para llamar el componente Persona Service*/
	@Autowired
	private IPersonaService service; 
	
	/*Objeto para llamar el componente Auditoria Service*/
	@Autowired
	private IAuditoriaService auditoriaService;
	
	/*Instacia Objeto MensajeDTO*/
	private MensajeDTO dto = new MensajeDTO();
	
	private ResponseEntity<MensajeDTO> entity = null;
	private HttpEntity<PersonasDTO> httpEntity = null;
	private HttpHeaders headers = new HttpHeaders();
	
	/*
	 * Metodo que consume el primer llamado hacia el microservicio, 
	 * el tal cuenta con la inserción hacia la tabla de auditoria
	 * 
	 */
	@GetMapping(value = "/hello-prueba", produces = "application/json")
	public ResponseEntity<MensajeDTO> helloPrueba(){
		dto.setMensaje(Constant.conexionExtablecida);
		dto.setMetodoConsumido("helloPrueba");
		entity = new ResponseEntity<MensajeDTO>(dto, Constant.statusExitoso);
		auditoriaService.transformarRespuesta(entity);
		return entity;
	}
	
	/*
	 * Metodo que consume el save, recibe como cuerpo un objeto Personas DTO, 
	 * tiene validacion de la cedula ya que es un campo requerido,
	 * realiza una busqueda por cedula y registra en la tabla de Auditoria tanto solicitud como respuesta
	 * 
	 */
	@PostMapping(value = "/registrar-personas", produces = "application/json")
	public ResponseEntity<MensajeDTO> registrarPersonas(@RequestBody PersonasDTO personasDTO){
		dto.setMetodoConsumido("registrarPersonas");
		try {
			headers.add("metodoConsumido", "registrarPersonas");
			httpEntity = new HttpEntity<PersonasDTO>(personasDTO, headers);
			auditoriaService.transformarSolicitud(httpEntity);
			if(personasDTO.getCedula().equals("")) {
				dto.setMensaje("La cedula es un campo obligatorio");
				entity = new ResponseEntity<MensajeDTO>(dto, Constant.statusError);
				auditoriaService.transformarRespuesta(entity);
			}else {
				Personas personas = service.listarPersonasPorCedula(personasDTO.getCedula());
				if (personas == null) {
					Personas personas2 = service.registrarPersonas(personasDTO);
					dto.setMensaje(Constant.registroCreado);
					entity = new ResponseEntity<MensajeDTO>(dto, Constant.statusCreado);
					auditoriaService.transformarRespuesta(entity);
				}else {
					dto.setMensaje(personas.getCedula() + " " + Constant.siHayResultado);
					entity = new ResponseEntity<MensajeDTO>(dto, Constant.statusError);
					auditoriaService.transformarRespuesta(entity);
				}
			}
		} catch (Exception e) {
			dto.setMensaje(String.valueOf(e.getCause()));
			entity = new ResponseEntity<MensajeDTO>(dto, Constant.statusError);
			auditoriaService.transformarRespuesta(entity);
		}
		return entity;
	}
	
	/*
	 * Metodo que consume el save, se valida que la cedula sea obligatoria,
	 * recibe como cuerpo un objeto PersonasDTO y un parametro que es el id de la persona que se quiere editar,
	 * tambien realiza inserción de solicitud y respuesta en la tabla de auditorias
	 * 
	 */
	@PutMapping(value = "/editar-personas", consumes = "application/json", produces = "application/json")
	public ResponseEntity<MensajeDTO> editarPersonas(@RequestBody PersonasDTO personasDTO, @RequestParam Integer id){
		dto.setMetodoConsumido("editarPersonas");
		try {
			personasDTO.setId(id);
			headers.add("metodoConsumido", "editarPersonas");
			httpEntity = new HttpEntity<PersonasDTO>(personasDTO, headers);
			auditoriaService.transformarSolicitud(httpEntity);
			if(personasDTO.getCedula().equals("")) {
				dto.setMensaje("La cedula es un campo obligatorio");
				entity = new ResponseEntity<MensajeDTO>(dto, Constant.statusError);
				auditoriaService.transformarRespuesta(entity);	
			}else {
				Personas personas = service.listarPorId(id);
				if(personas == null) {
					dto.setMensaje(Constant.noHayResultados);
					entity = new ResponseEntity<MensajeDTO>(dto, Constant.statusError);
					auditoriaService.transformarRespuesta(entity);
				}else {
					
					Personas personas2 = service.editarPersonas(personasDTO);
					dto.setMensaje(Constant.registroEditado);
					entity = new ResponseEntity<MensajeDTO>(dto, Constant.statusExitoso);
					auditoriaService.transformarRespuesta(entity);
				}
			}
			
		} catch (Exception e) {
			dto.setMensaje(String.valueOf(e.getCause()));
			entity = new ResponseEntity<MensajeDTO>(dto, Constant.statusError);
			auditoriaService.transformarRespuesta(entity);
		}
		return entity;
	}
	
	/*
	 * Metodo que consume delete, 
	 * tambien realiza inserción de solicitud y respuesta en la tabla de auditorias
	 * 
	 */
	@DeleteMapping(value = "/eliminar-personas", produces = "application/json")
	public ResponseEntity<MensajeDTO> eliminarPersonas(@RequestParam Integer id){
		dto.setMetodoConsumido("eliminarPersonas");
		try {
			dto.setMensaje(Constant.accionEliminar + String.valueOf(id));
			headers.add("metodoConsumido", "eliminarPersonas");
			HttpEntity<MensajeDTO> httpEntitys = new HttpEntity<MensajeDTO>(dto, headers);
			auditoriaService.transformarSolicitud(httpEntitys);
			service.eliminarPersonas(id);
			dto.setMensaje(Constant.registroEliminado);
			entity = new ResponseEntity<MensajeDTO>(dto, Constant.statusExitoso);
			auditoriaService.transformarRespuesta(entity);
		} catch (Exception e) {
			dto.setMensaje(e.getMessage());
			entity = new ResponseEntity<MensajeDTO>(dto, Constant.statusError);
			auditoriaService.transformarRespuesta(entity);
		}
		return entity;
	}
	
	/*
	 * Metodo que consume findAll, tiene validación de que si no encuentra registro devuelva un mensaje y si encuenta 
	 * devuelva la lista, tambien se realiza inserción de solicitud y respuesta en tabla de auditorias
	 * 
	 */
	@GetMapping(value = "/listar-todos", produces = "application/json")
	public ResponseEntity<Object> listarTodos(){
		dto.setMetodoConsumido("listarTodos");
		ResponseEntity<Object> retorno = null;
		try {
			headers.add("metodoConsumido", "listarTodos");
			dto.setMensaje("Busqueda por todos");
			HttpEntity<MensajeDTO> entity2 = new HttpEntity<MensajeDTO>(dto, headers);
			auditoriaService.transformarSolicitud(entity2);
			List<Personas> list = service.listarPersonas();
			if(list.size() > 0) {
				dto.setMensaje(Constant.siHayResultado + String.valueOf(list));
				entity = new ResponseEntity<MensajeDTO>(dto, Constant.statusExitoso);
				auditoriaService.transformarRespuesta(entity);
				retorno = new ResponseEntity<Object>(list, Constant.statusExitoso);
			}else {
				dto.setMensaje(Constant.noHayResultados);
				entity = new ResponseEntity<MensajeDTO>(dto, Constant.statusError);
				auditoriaService.transformarRespuesta(entity);
				retorno = new ResponseEntity<Object>(dto, Constant.statusError);
			}
		} catch (Exception e) {
			dto.setMensaje(e.getMessage());
			entity = new ResponseEntity<MensajeDTO>(dto, Constant.statusError);
			auditoriaService.transformarRespuesta(entity);
			retorno = new ResponseEntity<Object>(dto, Constant.statusError);
		}
		return retorno;
	}
	
	/*
	 * Metodo que consume findByCedula, tiene validación de que si no encuentra registro devuelva un mensaje y si encuenta 
	 * devuelva el objeto, tambien se realiza inserción de solicitud y respuesta en tabla de auditorias
	 * 
	 */
	@GetMapping(value = "/listar-cedula", produces = "application/json", consumes = "application/json")
	public ResponseEntity<Object> listarCedula(@RequestParam String cedula){
		dto.setMetodoConsumido("listarCedula");
		ResponseEntity<Object> retorno = null;
 		try {
			dto.setMensaje("Busqueda de Persona por Cedula:" + cedula);
			headers.add("metodoConsumido", "listarCedula");
			HttpEntity<MensajeDTO> entity2 = new HttpEntity<MensajeDTO>(dto, headers);
			auditoriaService.transformarSolicitud(entity2);
			Personas personas = service.listarPersonasPorCedula(cedula);
			if(personas == null) {
				dto.setMensaje(Constant.noHayResultados + cedula);
				entity = new ResponseEntity<MensajeDTO>(dto, Constant.statusExitoso);
				auditoriaService.transformarRespuesta(entity);
				retorno = new ResponseEntity<Object>(dto, Constant.statusError);
			}else {
				dto.setMensaje(Constant.siHayResultado + personas);
				entity = new ResponseEntity<MensajeDTO>(dto, Constant.statusExitoso);
				auditoriaService.transformarRespuesta(entity);
				retorno = new ResponseEntity<Object>(personas, Constant.statusExitoso);
			}
		} catch (Exception e) {
			dto.setMensaje(e.getMessage());
			entity = new ResponseEntity<MensajeDTO>(dto, Constant.statusError);
			auditoriaService.transformarRespuesta(entity);
			retorno = new ResponseEntity<Object>(dto, Constant.statusError);
		}
 		return retorno;
	}
}
