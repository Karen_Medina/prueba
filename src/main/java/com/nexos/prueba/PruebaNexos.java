package com.nexos.prueba;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

@SpringBootApplication
@EnableAsync
public class PruebaNexos {

	/*
	 * Metodo raíz para que la aplicación pueda desplegarse
	 * 
	 */
	public static void main(String[] args) {
		SpringApplication.run(PruebaNexos.class, args);
	}
	
	@Value("${pool.size:50}")
	private int poolSize;
	
	@Value("${pool.maxsize:100}")
	private int maxPoolSize;
	
	@Value("${queue.capacity:1000}")
	private int queueCapacity;
	
	/*
	 * Metodo para la configuración de la inserción asincrona 
	 * 
	 */
	@Bean(name="asyncExecutor")
	public TaskExecutor executor() {
		ThreadPoolTaskExecutor poolTaskExecutor = new ThreadPoolTaskExecutor();
		String info = "ThreadPoolTaskExecutor set [corePoolSize: " + poolSize + ", MaxPoolSize: " + maxPoolSize + ", QueueCapacity: " + queueCapacity + "]";
		Logger logger = LogManager.getLogger();
		poolTaskExecutor.setThreadNamePrefix("ascPrueba-");
		poolTaskExecutor.setCorePoolSize(poolSize);
		poolTaskExecutor.setMaxPoolSize(maxPoolSize);
		poolTaskExecutor.setQueueCapacity(queueCapacity);
		poolTaskExecutor.afterPropertiesSet();
		poolTaskExecutor.initialize();
		logger.info(info);
		return poolTaskExecutor;
	}

}
