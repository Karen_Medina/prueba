package com.nexos.prueba.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class PersonasDTO implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private Integer id;
	private String cedula;
	private String nombres;
	private String apellidos;
	private Integer edad;
	private String direccion;
	private String telefono;
	private String fechaNacimiento;
	private String nombreMadre;
	private String nombrePadre;
	
	@JsonProperty("roles")
	private RolesDTO rolesDTO;
}
