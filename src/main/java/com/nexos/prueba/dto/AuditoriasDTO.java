package com.nexos.prueba.dto;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;

@Data
public class AuditoriasDTO implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private Integer id;
	private String tipo;
	private Date fecha;
	private String metodoConsumido;
	private String trama;
	private String estado;
}
