package com.nexos.prueba.dto;

import java.io.Serializable;

import lombok.Data;

@Data
public class RolesDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Integer id;
	private String nombreRol;

}
