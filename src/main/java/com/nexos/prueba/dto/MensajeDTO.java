package com.nexos.prueba.dto;

import lombok.Data;

@Data
public class MensajeDTO {

	private String mensaje;
	private String metodoConsumido;
}
