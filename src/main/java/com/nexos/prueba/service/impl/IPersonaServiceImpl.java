package com.nexos.prueba.service.impl;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nexos.prueba.dao.INucleoDAO;
import com.nexos.prueba.dao.IPersonasDAO;
import com.nexos.prueba.dto.PersonasDTO;
import com.nexos.prueba.model.Nucleo;
import com.nexos.prueba.model.Personas;
import com.nexos.prueba.service.IPersonaService;

@Service
@Transactional
public class IPersonaServiceImpl implements IPersonaService{
	
	@Autowired
	private IPersonasDAO dao;
	
	@Autowired
	private INucleoDAO nucleodao;

	/*
	 * Metodo que realiza insert en las tablas Nucleos y Personas, 
	 * hace un llamado al metodo para llenar la entidad Nucleos
	 * 
	 */
	@Override
	public Personas registrarPersonas(PersonasDTO personasDTO) {
		Nucleo nucleo = transformarNucleo(personasDTO);
		return dao.save(new Personas(personasDTO));
	}

	/*
	 * Metodo que realiza update en las tablas Nucleos y Personas, 
	 * hace un llamado al metodo para editar la entidad Nucleos
	 * 
	 */
	@Override
	public Personas editarPersonas(PersonasDTO personasDTO) {
		Nucleo nucleo = transformarNucleo(personasDTO);
		Personas personas = new Personas(personasDTO);
		return dao.save(personas);
	}

	/*
	 * Metodo que realiza busqueda en la tabla Personas, 
	 * 
	 */
	@Override
	public List<Personas> listarPersonas() {
		return dao.findAll();
	}

	/*
	 * Metodo que realiza select en la tabla Personas, 
	 * recibiendo como parametro la cedula de la persona
	 */
	@Override
	public Personas listarPersonasPorCedula(String cedula) {
		return dao.findByCedula(cedula);
	}

	/*
	 * Metodo que realiza delete en la tabla Personas, 
	 * recibiendo como parametro el id de la persona,
	 * tambien realiza delete en la tabla Nucleos
	 */
	@Override
	public void eliminarPersonas(Integer id) {
		Personas personas = dao.findByid(id);
		String hijo = personas.getNombres() + " " + personas.getApellidos();
		Nucleo nucleo = nucleodao.findByHijo(hijo);
		nucleodao.deleteById(nucleo.getId());
		dao.deleteById(id);
	}

	/*
	 * Metodo que realiza select en la tabla Personas, 
	 * recibiendo como parametro el id de la persona
	 */
	@Override
	public Personas listarPorId(Integer id) {
		return dao.findByid(id);  
	}
	
	/*
	 * Metodo que recibe un DTO y obtiene los parametros necesarios para llenar la entidad Nucleos,
	 * tambien realiza select en la tabla Nucleos recibiendo como parametro el hijo,
	 * dependiendo su resultado realiza insert o update 
	 */
	public Nucleo transformarNucleo(PersonasDTO personasDTO) {
		Nucleo nucleo = new Nucleo();
		nucleo.setMadre(personasDTO.getNombreMadre());
		nucleo.setPadre(personasDTO.getNombrePadre());
		nucleo.setHijo(personasDTO.getNombres() + " " + personasDTO.getApellidos());
		Nucleo nucleo3 = nucleodao.findByHijo(nucleo.getHijo());
		if(nucleo3 == null) {
			Nucleo nucleo2 = nucleodao.save(nucleo);
			return nucleo2;
		}else {
			nucleo.setId(nucleo3.getId());
			Nucleo nucleo4 = nucleodao.save(nucleo);
			return nucleo4;
		}
	}

	
}
 