package com.nexos.prueba.service.impl;

import java.sql.Date;
import java.util.concurrent.CompletableFuture;

import javax.transaction.Transactional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.nexos.prueba.constant.Constant;
import com.nexos.prueba.dao.IAuditoriasDAO;
import com.nexos.prueba.dto.AuditoriasDTO;
import com.nexos.prueba.dto.MensajeDTO;
import com.nexos.prueba.model.Auditorias;
import com.nexos.prueba.service.IAuditoriaService;

@Service
@Transactional
public class IAuditoriasServiceImpl implements IAuditoriaService {
	
	@Autowired
	private IAuditoriasDAO dao;
	
	private Gson gson = new Gson();

	/*
	 * Metodo asincrono que realiza la inserción a la tabla Auditorias
	 * 
	 */
	@Override
	@Async("asyncExecutor")
	public CompletableFuture<Auditorias> registarAuditoriaAsync(Auditorias t) throws InterruptedException {
		Auditorias auditorias = new Auditorias();
		try {
			auditorias = dao.save(t);
			Logger logger = LogManager.getLogger();
			String info = "save" + Thread.currentThread().getName();
			logger.info(info);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return CompletableFuture.completedFuture(auditorias);
	}
	
	/*
	 * Metodo que convierte AuditoriasDTO a una entidad y llama el metodo asincrono para el consumo
	 * 
	 */
	public CompletableFuture<Auditorias> recibirAuditoria(AuditoriasDTO auditoriasDTO) throws InterruptedException{
		Auditorias auditorias = new Auditorias(auditoriasDTO);
		registarAuditoriaAsync(auditorias);
		return CompletableFuture.completedFuture(auditorias);
	}

	/*
	 * Metodo que transforma la petición de solicitud en AuditoriasDTO para luego,
	 * realizar un llamado al metodo que transforma un DTO en entidad
	 * 
	 */
	@Override
	public AuditoriasDTO transformarSolicitud(HttpEntity entity) {
		AuditoriasDTO auditoriasDTO = new AuditoriasDTO();
		try {
			auditoriasDTO.setTipo(Constant.solicitud);
			auditoriasDTO.setFecha(Date.valueOf(Constant.fecha));
			String body = gson.toJson(entity.getBody());
			auditoriasDTO.setTrama(body);
			auditoriasDTO.setMetodoConsumido(entity.getHeaders().getFirst("metodoConsumido"));
			auditoriasDTO.setEstado(String.valueOf(Constant.statusAceptado));
			recibirAuditoria(auditoriasDTO);
		} catch (InterruptedException e) {
			System.out.println(e);
		}
		return auditoriasDTO;
	}
	
	/*
	 * Metodo que transforma la petición de respuesta en AuditoriasDTO para luego,
	 * realizar un llamado al metodo que transforma un DTO en entidad
	 * 
	 */
	@Override
	public AuditoriasDTO transformarRespuesta(ResponseEntity responseEntity) {
		MensajeDTO dto = (MensajeDTO) responseEntity.getBody();
		AuditoriasDTO auditoriasDTO = new AuditoriasDTO();
		try {
			auditoriasDTO.setTipo(Constant.respuesta);
			auditoriasDTO.setFecha(Date.valueOf(Constant.fecha));
			auditoriasDTO.setEstado(String.valueOf(responseEntity.getStatusCode()));
			String body = gson.toJson(dto);
			auditoriasDTO.setTrama(body);
			auditoriasDTO.setMetodoConsumido(dto.getMetodoConsumido());
			recibirAuditoria(auditoriasDTO);
		} catch (InterruptedException e) {
			System.out.println(e);
		}
		return auditoriasDTO;
	}

	

	
}
