package com.nexos.prueba.service;

import java.util.List;

import org.springframework.stereotype.Component;

import com.nexos.prueba.dto.PersonasDTO;
import com.nexos.prueba.model.Personas;

/*
 * Interfaz que contiene los metodos que va a heredar la clase IAuditoriaServiceImpl y se van a implementar
 * 
 */
@Component
public interface IPersonaService {

	Personas registrarPersonas(PersonasDTO personasDTO);
	
	Personas editarPersonas(PersonasDTO personasDTO);
	
	List<Personas> listarPersonas();
	
	Personas listarPersonasPorCedula(String cedula);
	
	Personas listarPorId(Integer id);
	
	void eliminarPersonas(Integer id);
}
