package com.nexos.prueba.service;

import java.util.concurrent.CompletableFuture;

import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import com.nexos.prueba.dto.AuditoriasDTO;
import com.nexos.prueba.model.Auditorias;

/*
 * Interfaz que contiene los metodos que va a heredar la clase IPersonaServiceImpl y se van a implementar
 * 
 */

@Component
public interface IAuditoriaService {

	@Async("asyncExecutor")
	CompletableFuture<Auditorias> registarAuditoriaAsync(Auditorias t) throws InterruptedException;
	
	AuditoriasDTO transformarSolicitud(HttpEntity<?> entity);
	
	AuditoriasDTO transformarRespuesta(ResponseEntity<?> responseEntity);
}
