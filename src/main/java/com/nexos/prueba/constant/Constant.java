package com.nexos.prueba.constant;

import java.io.Serializable;
import java.time.LocalDate;

import org.springframework.http.HttpStatus;

/*
 * Clase que contiene los mensajes y estados mas frecuentes utilizados
 * 
 */
public class Constant implements Serializable {

	private static final long serialVersionUID = 1L;
	
	public static final String conexionExtablecida = "Conexión Establecida Prueba Nexos";
	public static final String registroCreado = "Registro Creado Correctamente";
	public static final String registroEditado = "Registro Editado Correctamente";
	public static final String registroEliminado = "Registro Eliminado Correctamente";
	public static final String noHayResultados = "No se encuentran resultados de la busqueda";
	public static final String siHayResultado = "Ya se encuentra registrado en la base de datos";
	public static final String accionEliminar = "Se va a eliminar el elemento con el id:";
	public static final String solicitud = "REQUEST";
	public static final String respuesta = "RESPONSE";
	
	public static final LocalDate fecha = LocalDate.now();
	public static final HttpStatus statusAceptado = HttpStatus.ACCEPTED;
	public static final HttpStatus statusExitoso = HttpStatus.OK;
	public static final HttpStatus statusCreado = HttpStatus.CREATED;
	public static final HttpStatus statusError = HttpStatus.INTERNAL_SERVER_ERROR;
	
	

}
